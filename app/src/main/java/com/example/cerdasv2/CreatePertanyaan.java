package com.example.cerdasv2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.cerdasv2.Models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CreatePertanyaan extends AppCompatActivity {
    String pertanyaan;
    TextView _pertanyaan;
    Button _submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pertanyaan);

        initializeUI();

        _submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tambahPertanyaan();
            }
        });
    }

    private void tambahPertanyaan(){
        pertanyaan = _pertanyaan.getText().toString();
        User user = new User();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("forum").child(user.getUid());

        DatabaseReference pertanyaanRef = myRef.child("pertanyaan");
        pertanyaanRef.setValue(pertanyaan);
        myRef.child("notifikasi_status").setValue(false);
        myRef.child("uid").setValue(FirebaseAuth.getInstance().getUid());

        DiskusiActivity.da.finish();
        startActivity(new Intent(getApplicationContext(), DiskusiActivity.class));
        finish();
    }

    private void initializeUI(){
        _pertanyaan = findViewById(R.id.pertanyaan);
        _submit = findViewById(R.id.submit);
    }
}
