package com.example.cerdasv2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditProfilActivity extends AppCompatActivity {
    EditText edit_nama;
    RadioButton pria, wanita;

    Button Submit;
    String gender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);

        pria = findViewById(R.id.edit_profile_wanita);
        wanita = findViewById(R.id.edit_profile_pria);
        edit_nama = findViewById(R.id.edit_profile_name);
        Submit = findViewById(R.id.edit_profile_submit);

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("profile").child(FirebaseAuth.getInstance().getUid());

                myRef.child("nama").setValue(edit_nama.getText().toString());
                myRef.child("jenis_kelamin").setValue(gender);
                ProfileActivity.pa.finish();
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                finish();
            }
        });
    }

    public void genderClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()){
            case R.id.edit_profile_pria:
                if (checked)
                    gender = "Pria";
                break;
            case R.id.edit_profile_wanita:
                if (checked)
                    gender = "Wanita";
                break;
        }
    }
}
