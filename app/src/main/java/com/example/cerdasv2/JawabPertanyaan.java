package com.example.cerdasv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cerdasv2.Models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class JawabPertanyaan extends AppCompatActivity {
    String pertanyaanText, jawaban, uid;
    EditText _jawaban;
    TextView _pertanyaanText;
    Button _submitJawaban;
    ListView listView;
    ArrayList<String> arrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jawab_pertanyaan);

        pertanyaanText = getIntent().getStringExtra("pertanyaanText");
        _pertanyaanText = findViewById(R.id.textPertanyaan);
        _pertanyaanText.setText(pertanyaanText);

        _jawaban = findViewById(R.id.jawaban);
        _submitJawaban = findViewById(R.id.submitJawaban);

        _submitJawaban.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitJawaban();
            }
        });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("forum");

         myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void  onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();
                String data;
                while (iterator.hasNext()) {
                    DataSnapshot next = (DataSnapshot) iterator.next();
                    data = next.child("pertanyaan").getValue().toString();
                    if(data.equals(getIntent().getStringExtra("pertanyaanText"))){
                        uid = next.getKey();
                    }
                }
                getAnswer();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        DatabaseReference uidRef = FirebaseDatabase.getInstance().getReference().child("forum");
//        uidRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                Iterable<DataSnapshot> sIterator = dataSnapshot.getChildren();
//                Iterator<DataSnapshot> iterators = sIterator.iterator();
//                String uids;
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });


    }

    public void submitJawaban(){
        User user = new User();
        String uids;

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("forum");

        jawaban = _jawaban.getText().toString();
        Toast.makeText(getApplicationContext(), uid, Toast.LENGTH_LONG).show();

        DatabaseReference jawabRef = myRef.child(uid).child("jawaban").child(user.getUid()).child("jawaban_text");
        jawabRef.setValue(jawaban);
        myRef.child(uid).child("notifikasi_status").setValue(true);
        DiskusiActivity.da.finish();
        startActivity(new Intent(getApplicationContext(), DiskusiActivity.class));
        finish();
    }

    public void getAnswer(){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("forum").child(uid).child("jawaban");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);

        listView = (ListView) findViewById(R.id.listViewJawaban);
        listView.setAdapter(arrayAdapter);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();
                String data;
                while (iterator.hasNext()) {
                    DataSnapshot next = (DataSnapshot) iterator.next();
                    data = next.child("jawaban_text").getValue().toString();
                    arrayList.add(data);
                }
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
