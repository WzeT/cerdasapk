package com.example.cerdasv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.cerdasv2.Models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Iterator;

public class MainActivity extends AppCompatActivity {
    private Button logout, profile, diskusi, cari_member;
    private ProgressDialog pd ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pd = new ProgressDialog(this);
        pd.setMessage("Memproses");
//        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        pd.setIndeterminate(true);

        logout = findViewById(R.id.logout);
        diskusi = findViewById(R.id.diskusi);
        profile = findViewById(R.id.profil);
        cari_member = findViewById(R.id.cari_member);

        diskusi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), DiskusiActivity.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user.logOut();
                finish();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = FirebaseAuth.getInstance().getUid().toString();
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                intent.putExtra("user_id", text);
                startActivity(intent);
            }
        });

        cari_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), PertanyaanSayaActivity.class));
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("forum").child(FirebaseAuth.getInstance().getUid());

                pd.show();
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            String test = dataSnapshot.child("notifikasi_status").getValue().toString();

                            if (test.toString().equals("true")){
                                startActivity(new Intent(getApplicationContext(), PertanyaanSayaActivity.class));
                                pd.dismiss();
                            }else{
                                Toast.makeText(getApplicationContext(), "belum Ada pertanyaan yang dijawab", Toast.LENGTH_LONG).show();
                                pd.dismiss();
                            }
                        } else{
                            Toast.makeText(getApplicationContext(), "belum Ada Pertanyaan yang dijawab", Toast.LENGTH_LONG).show();
                            pd.dismiss();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

    }
}
