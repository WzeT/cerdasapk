package com.example.cerdasv2.Models;

public class Forum {
    private String key, pertanyaan, profilid, topik;

    public Forum(String key, String pertanyaan, String profilid, String topik) {
        this.key = key;
        this.pertanyaan = pertanyaan;
        this.profilid = profilid;
        this.topik = topik;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getProfilid() {
        return profilid;
    }

    public void setProfilid(String profilid) {
        this.profilid = profilid;
    }

    public String getTopik() {
        return topik;
    }

    public void setTopik(String topik) {
        this.topik = topik;
    }

    @Override
    public String toString() {
        return "Forum{" +
                "key='" + key + '\'' +
                ", pertanyaan='" + pertanyaan + '\'' +
                ", profilid='" + profilid + '\'' +
                ", topik='" + topik + '\'' +
                '}';
    }
}
