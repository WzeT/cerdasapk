package com.example.cerdasv2.Models;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class User {
    String email, uid;
    FirebaseAuth auth = FirebaseAuth.getInstance();

    public String getUid(){
        uid = auth.getUid();
        return uid;
    }
    public void logOut(){
        auth.signOut();
    }
    public String getEmail() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("profile").child(getUid()).child("email");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                email = dataSnapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return email;
    }
}
