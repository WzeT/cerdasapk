package com.example.cerdasv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class PertanyaanSayaActivity extends AppCompatActivity {
    ArrayList<String> arrayList = new ArrayList<>();
    ListView listView;
    ProgressDialog pgDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertanyaan_saya);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("forum");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);

        listView = (ListView) findViewById(R.id.pertanyaan_di_jawab);
        listView.setAdapter(arrayAdapter);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();
                String data, uidPenanya;
                while (iterator.hasNext()) {
                    DataSnapshot next = (DataSnapshot) iterator.next();
                    data = next.child("pertanyaan").getValue().toString();
                    uidPenanya = next.child("uid").getValue().toString();
                    if(uidPenanya.equals(FirebaseAuth.getInstance().getUid())){
                        arrayList.add(data);
                    }
                }
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = arrayAdapter.getItem(position);
                Intent intent = new Intent(getApplicationContext(), JawabPertanyaan.class);

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("forum").child(FirebaseAuth.getInstance().getUid());
                ref.child("notifikasi_status").setValue(false);

                intent.putExtra("pertanyaanText", text);
                startActivity(intent);
                finish();
            }
        });
    }
}
