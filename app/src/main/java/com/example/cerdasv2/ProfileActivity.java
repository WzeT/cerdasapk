package com.example.cerdasv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;

public class ProfileActivity extends AppCompatActivity {
    TextView profile_email, profile_name, profile_gender;
    Button edit_profile;
    String uid;
    public static ProfileActivity pa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        uid = FirebaseAuth.getInstance().getUid().toString();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("profile").child(uid);
        pa = this;
        profile_email = findViewById(R.id.profile_email);
        profile_gender = findViewById(R.id.profile_gender);
        profile_name = findViewById(R.id.profile_nama);

        profile_email.setText(getIntent().getStringExtra("user_id"));

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = snapshotIterator.iterator();

                profile_email.setText(dataSnapshot.child("email").getValue().toString());
                profile_name.setText(dataSnapshot.child("nama").getValue().toString());
                profile_gender.setText(dataSnapshot.child("jenis_kelamin").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        edit_profile = findViewById(R.id.profile_edit_button);
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), EditProfilActivity.class));
            }
        });
    }
}
